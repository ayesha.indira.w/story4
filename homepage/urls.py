from django.urls import path
from django.conf.urls import include
from . import views

app_name='homepage'

urlpatterns = [
    path('',views.home,name='home'),
    path('about/',views.about,name='about'),
    path('contact/',views.contact,name='contact'),
    path('schedule/',include("schedule.urls")),
]
