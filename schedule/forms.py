from django import forms


class SchedForm(forms.Form):
    Name = forms.CharField(label="Name", 
        widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Name',
        'type' : 'text',
        'required': True,
    }))

    Lecturer = forms.CharField(label="Lecturer", 
        widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Lecturer',
        'type' : 'text',
        'required': True,
    }))

    #SKS = forms.CharField(label="SKS",
     #   widget=forms.TextInput(attrs={
      #  'class': 'form-control',
       # 'placeholder': 'SKS',
        #'type' : 'text',
        #'required': True,
    #}))
    
    Description = forms.CharField(label="Description", 
        widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Description',
        'type' : 'text',
        'required': True,
    }))

    Semester = forms.CharField(label="Semester", 
        widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester',
        'type' : 'text',
        'required': True,
    }))

    Class = forms.CharField(label="Class", 
        widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Ruang Kelas',
        'type' : 'text',
        'required': True,
    }))
