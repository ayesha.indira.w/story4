from django.shortcuts import render, redirect
from .models import Schedule
from .forms import SchedForm

# Create your views here.
def join(request):
    if request.method == "POST":
        form = SchedForm(request.POST)
        if form.is_valid():
            sched = Schedule(
            Name = form.cleaned_data['Name'],
            Lecturer = form.cleaned_data['Lecturer'],
            # sched.SKS = form.cleaned_data['SKS']
            Description = form.cleaned_data['Description'],
            Semester = form.cleaned_data['Semester'],
            Class = form.cleaned_data['Class'],
            )
            sched.save()
        return redirect('/schedule')
    else:
        #sched = Schedule.objects.all()
        form = SchedForm()
        response = {'form':form}
        print(response)
        return render(request,'Schedule.html', response)

def delete(request,pk):
    if request.method == "POST":
        pk = request.POST['pk']
        Schedule.objects.get(pk=pk).delete()
        return redirect('/schedule')
    else:
        sched = Schedule.objects.all()
        form = SchedForm()
        response = {"sched":sched, 'form':form}
        return render(request,'Schedule.html', response)

def see(request,pk):
    if request.method=="POST":
        pk=request.POST['pk']
        sched = Schedule.objects.get(pk=pk)
        response= {"sched":sched}
        return render(request, 'isi.html', response)
