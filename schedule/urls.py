from django.urls import path
from .views import join, delete, see

app_name='schedule'

urlpatterns = [
    path('', join, name='join'),
    path('see/delete/<int:pk>',delete),
    path('see/sched/<int:pk>',see),
]
